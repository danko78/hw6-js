function filterBy(array, typeOfData) {
    let result = array.filter(item => typeof item !== typeOfData);
    return result;
}

console.log(filterBy(['hello', 'world', 23, '23', null], "string"));